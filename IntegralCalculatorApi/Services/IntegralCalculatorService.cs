﻿using IntegralCalculatorApi.Entities;
using IntegralCalculatorApi.Extensions;
using MathNet.Numerics.Integration;
using MathNet.Symbolics;

namespace IntegralCalculatorApi.Services
{
    public class IntegralCalculatorService
    {
        public static double Calculate(IntegralRequestBody requestBody, WebApplication app)
        {
            ILogger<IntegralCalculatorService> logger = app.GetLogger<IntegralCalculatorService>();

            Func<double, double>? functionDeserialized = default;

            try
            {
                functionDeserialized = SymbolicExpression.Parse(requestBody.Function).Compile("x");
            }
            catch (Exception ex)
            {
                logger.LogError($"Couldn't parse the given function." +
                    $"\nFunction: {requestBody.Function}");
                logger.LogError($"Exception: {ex.Message}");
                throw;
            }

            try
            {
                return SimpsonRule.IntegrateComposite(functionDeserialized, requestBody.IntegralBegin.Value, requestBody.IntegralEnd.Value, requestBody.NumberOfPartisions.Value);
            }
            catch (Exception ex)
            {
                logger.LogError($"Couldn't calculate integral with the given parameters." +
                    $"\nParameters:" +
                    $"\nBegin: {requestBody.IntegralBegin.Value}" +
                    $"\nEnd: {requestBody.IntegralEnd.Value}" +
                    $"\nPartisions: {requestBody.NumberOfPartisions}");
                logger.LogError($"Exception: {ex.Message}");
                throw;
            }
        }
    }
}
