using IntegralCalculatorApi.Entities;
using IntegralCalculatorApi.EndpointDefinitions;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddCors(options => options.AddPolicy("allowAny", o =>
{
    o.AllowAnyOrigin();
    o.AllowAnyHeader();
    o.AllowAnyMethod();
}));

builder.Services.AddEndpointDefinitions(typeof(IntegralRequestBody));

var app = builder.Build();

app.UseCors("allowAny");

app.UseEndpointDefinitions();

app.Run();
